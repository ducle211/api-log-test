/* eslint-disable */
const { Client } = require('@elastic/elasticsearch');
var faker = require('faker');

const client = new Client({ node: 'https://gamify-elasticsearch-prestg--3tlrim0.wonderfulriver-cbc1bccb.eastus.azurecontainerapps.io' });

for (let i = 0; i < 100; i++) {
  const transferTransactions = [];

  for (let i = 0; i < 10; i++) {
    transferTransactions.push(
      flattenObject({
        id: faker.datatype.uuid(),
        type: faker.random.arrayElement(['DEPOSIT', 'WITHDRAW']),
        amount: faker.finance.amount(),
        status: faker.random.arrayElement(['COMPLETED', 'CANCELLED']),
        note: faker.lorem.sentence(),
        player: {
          id: faker.datatype.uuid(),
          nativeId: faker.datatype.uuid(),
        },
        group: {
          id: faker.datatype.uuid(),
          name: faker.company.companyName(),
        },
        brand: {
          id: faker.datatype.uuid(),
          name: faker.company.companyName(),
        },
        wallet: {
          id: faker.datatype.uuid(),
          balance: faker.finance.amount(),
        },
        currency: {
          id: faker.datatype.uuid(),
          name: faker.finance.currencyName(),
          code: faker.finance.currencyCode(),
          symbol: faker.finance.currencySymbol(),
          prefix: 1,
        },
        created: faker.date.past().toISOString(),
      }),
    );
  }

  const body = transferTransactions.flatMap((doc) => [
    { index: { _index: 'transfer_transactions' } },
    doc,
  ]);

  client.bulk({ refresh: true, body });
}

function flattenObject(ob) {
  const toReturn = {};

  for (const i in ob) {
    if (!Object.prototype.hasOwnProperty.call(ob, i)) continue;

    if (typeof ob[i] == 'object' && ob[i] !== null) {
      const flatObject = flattenObject(ob[i]);
      for (const x in flatObject) {
        if (!Object.prototype.hasOwnProperty.call(flatObject, x)) continue;

        toReturn[i + '.' + x] = flatObject[x];
      }
    } else {
      toReturn[i] = ob[i];
    }
  }
  return toReturn;
}
