/* eslint-disable */
const { Client } = require('@elastic/elasticsearch');
const cliProgress = require('cli-progress');

const client = new Client({ node: 'http://localhost:9200' });
const knex = require('knex')({
  client: 'mysql',
  connection: {
    host: '127.0.0.1',
    port: 3306,
    user: 'root',
    password: '',
    database: 'gamify-bo',
  },
});

async function synBets() {
  const bar = new cliProgress.SingleBar({}, cliProgress.Presets.shades_classic);

  const total = await knex.count('id as count').from('bet').first();
  let count = 0;
  let rep = 1000;

  bar.start(total.count, 0, {
    speed: 'N/A',
  });

  while (count < total.count + rep) {
    await knex('bet')
      .join('player', 'bet.playerId', 'player.id')
      .join('game', 'bet.gameId', 'game.id')
      .join('currency', 'bet.currencyId', 'currency.id')
      .select(
        'bet.id as id',
        'bet.docStatus as docStatus',
        'bet.created as created',
        'bet.updated as updated',
        'bet.buildIn as buildIn',
        'bet.groupId as groupId',
        'bet.brandId as brandId',
        'amount',
        'earn',
        'betType',
        'parentBetId',
        'spinResult',
        'winData',
        'betSize',
        'betLevel',
        'playerBalance',
        'betStatus',
        'channel',
        'gameConfig',
        'playUrl',
        'player.id as player.id',
        'player.nativeId as player.nativeId',
        'player.balance as player.balance',
        'player.playerToken as player.playerToken',
        'player.isDemoPlayer as player.isDemoPlayer',
        'player.isExpireFun as player.isExpireFun',
        'game.id as game.id',
        'game.name as game.name',
        'game.gameId as game.gameId',
        'game.docStatus as game.docStatus',
        'currency.id as currency.id',
        'currency.name as currency.name',
        'currency.symbol as currency.symbol',
        'currency.code as currency.code',
        'currency.prefix as currency.prefix ',
        'currency.docStatus as currency.docStatus',
      )
      .offset(count)
      .limit(rep)
      .then(async (bets) => {
        bets = bets.map((bet) => {
          bet.spinResult = Buffer.from(JSON.stringify(bet.spinResult)).toString('base64');
          bet.winData = !bet.winData
            ? null
            : Buffer.from(JSON.stringify(bet.winData)).toString('base64');
          bet.gameConfig = Buffer.from(JSON.stringify(bet.gameConfig)).toString('base64');
          return Object.assign({}, bet);
        });

        if (bets.length) {
          const body = bets.flatMap((doc) => [{ index: { _index: 'transactions' } }, doc]);
          await client.bulk({ refresh: true, body });
        }
      });
    bar.update(count);
    count += rep;
  }
  bar.stop();
  console.log('DONE!');
}

(async () => {
  await synBets();
})();
