/* eslint-disable */
const { Client } = require('@elastic/elasticsearch');
var faker = require('faker');

const client = new Client({ node: 'https://gamify-elasticsearch-prestg--3tlrim0.wonderfulriver-cbc1bccb.eastus.azurecontainerapps.io' });

for (let i = 0; i < 100; i++) {
  const seamlessTransactions = [];

  for (let i = 0; i < 10; i++) {
    seamlessTransactions.push(
      flattenObject({
        id: faker.datatype.uuid(),
        type: faker.random.arrayElement(['PLACE_BET', 'PAYOUT']),
        status: faker.random.arrayElement(['COMPLETED', 'FAILED']),
        player: {
          id: faker.datatype.uuid(),
          nativeId: faker.datatype.uuid(),
          playerToken: faker.datatype.uuid(),
        },
        group: {
          id: faker.datatype.uuid(),
          name: faker.company.companyName(),
        },
        brand: {
          id: faker.datatype.uuid(),
          name: faker.company.companyName(),
        },
        currency: {
          id: faker.datatype.uuid(),
          name: faker.finance.currencyName(),
          code: faker.finance.currencyCode(),
          symbol: faker.finance.currencySymbol(),
          prefix: 1,
        },
        game: {
          id: faker.datatype.uuid(),
          name: faker.company.companyName(),
          gameId: Math.random().toString(36).substr(2, 4).toUpperCase(),
          gameCode: faker.datatype.number({ min: 1000, max: 9999 }).toString(),
        },
        bet: {
          id: faker.datatype.uuid(),
          amount: faker.finance.amount(),
          parentBetId: faker.datatype.uuid(),
        },

        res: {
          code: faker.random.arrayElement([200, 400, 500]),
          message: faker.lorem.sentence(),
          balance: faker.finance.amount(),
          currency: faker.finance.currencyCode(),
        },

        created: faker.date.past().toISOString(),
      }),
    );
  }

  const body = seamlessTransactions.flatMap((doc) => [
    { index: { _index: 'seamless_transactions' } },
    doc,
  ]);

  client.bulk({ refresh: true, body });
}

function flattenObject(ob) {
  const toReturn = {};

  for (const i in ob) {
    if (!Object.prototype.hasOwnProperty.call(ob, i)) continue;

    if (typeof ob[i] == 'object' && ob[i] !== null) {
      const flatObject = flattenObject(ob[i]);
      for (const x in flatObject) {
        if (!Object.prototype.hasOwnProperty.call(flatObject, x)) continue;

        toReturn[i + '.' + x] = flatObject[x];
      }
    } else {
      toReturn[i] = ob[i];
    }
  }
  return toReturn;
}
