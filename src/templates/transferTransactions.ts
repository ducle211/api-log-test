import { TRANSFER_TRANSACTION_INDEX } from '../app/transaction/constants';

export default {
  name: TRANSFER_TRANSACTION_INDEX,
  _metadata: {
    base64Fields: [],
  },
  body: {
    index_patterns: [`${TRANSFER_TRANSACTION_INDEX}_*`],
    template: {
      settings: {
        number_of_shards: 10,
        'index.max_result_window': 1000000,
      },
      mappings: {
        properties: {
          id: { type: 'keyword', index: true },
          nativeTransactionId: { type: 'keyword', index: true },
          type: { type: 'keyword', index: true },
          amount: { type: 'float', index: true },
          status: { type: 'keyword', index: true },
          note: { type: 'text', index: false },

          //Player
          'player.id': { type: 'keyword', index: true },
          'player.nativeId': { type: 'keyword', index: true },

          //Group
          'group.id': { type: 'keyword', index: true },
          'group.name': { type: 'keyword', index: true },

          //Brand
          'brand.id': { type: 'keyword', index: true },
          'brand.name': { type: 'keyword', index: true },

          //Wallet
          'wallet.id': { type: 'keyword', index: true },
          'wallet.balance': { type: 'float', index: false },

          //Currency
          'currency.id': { type: 'keyword', index: true },
          'currency.name': { type: 'keyword', index: true },
          'currency.code': { type: 'keyword', index: true },
          'currency.symbol': { type: 'keyword', index: true },
          'currency.prefix': { type: 'integer', index: false },

          created: { type: 'date', index: true },
        },
      },
    },
  },
};
