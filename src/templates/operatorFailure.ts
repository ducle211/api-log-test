import { OPERATOR_FAILURE_INDEX } from '../app/transaction/constants';

export default {
  name: OPERATOR_FAILURE_INDEX,
  _metadata: {
    base64Fields: [],
  },
  body: {
    index_patterns: [`${OPERATOR_FAILURE_INDEX}_*`],
    template: {
      settings: {
        number_of_shards: 10,
        'index.max_result_window': 1000000,
      },
      mappings: {
        properties: {
          id: { type: 'keyword', index: true },
          nativeId: { type: 'keyword', index: true },
          nativeTransactionId: { type: 'keyword', index: true },
          brandCode: { type: 'keyword', index: true },
          groupCode: { type: 'keyword', index: true },
          currencyCode: { type: 'keyword', index: true },
          amount: { type: 'float', index: false },
          note: { type: 'text', index: false },
        },
      },
    },
  },
};
