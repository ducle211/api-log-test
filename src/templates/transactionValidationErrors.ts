import { TRANSACTION_VALIDATION_ERRORS } from '../app/transaction/constants';

export default {
  name: TRANSACTION_VALIDATION_ERRORS,
  _metadata: {
    base64Fields: [],
  },
  body: {
    index_patterns: [`${TRANSACTION_VALIDATION_ERRORS}_*`],
    template: {
      settings: {
        number_of_shards: 10,
        'index.max_result_window': 1000000,
      },
      mappings: {
        properties: {
          type: { type: 'keyword', index: true },
          message: { type: 'text', index: false },
          payload: { type: 'keyword', index: true },
        },
      },
    },
  },
};
