import { SEAMLESS_RECONCILIATION_INDEX } from '../app/transaction/constants';

export default {
  name: SEAMLESS_RECONCILIATION_INDEX,
  _metadata: {
    base64Fields: [],
  },
  body: {
    index_patterns: [`${SEAMLESS_RECONCILIATION_INDEX}_*`],
    template: {
      settings: {
        number_of_shards: 10,
        'index.max_result_window': 1000000,
      },
      mappings: {
        properties: {
          id: { type: 'keyword', index: true },
          type: { type: 'keyword', index: true },
          transactionId: { type: 'keyword', index: true },

          // Bet
          'bet.id': { type: 'keyword', index: true },
          'bet.amount': { type: 'float', index: false },
          'bet.parentBetId': { type: 'keyword', index: true },

          // Game
          'game.id': { type: 'keyword', index: true },
          'game.name': { type: 'keyword', index: true },
          'game.gameId': { type: 'keyword', index: true },
          'game.gameCode': { type: 'keyword', index: true },

          //Group
          'group.id': { type: 'keyword', index: true },
          'group.name': { type: 'keyword', index: true },

          //Brand
          'brand.id': { type: 'keyword', index: true },
          'brand.name': { type: 'keyword', index: true },

          //Player
          'player.id': { type: 'keyword', index: true },
          'player.nativeId': { type: 'keyword', index: true },
          'player.playerToken': { type: 'keyword', index: true },

          //Currency
          'currency.id': { type: 'keyword', index: true },
          'currency.name': { type: 'keyword', index: true },
          'currency.code': { type: 'keyword', index: true },
          'currency.symbol': { type: 'keyword', index: true },
          'currency.prefix': { type: 'integer', index: false },

          payload: { type: 'text', index: false },

          retryCounter: { type: 'byte', index: false },

          status: { type: 'keyword', index: true },

          created: { type: 'date', index: true },

          updated: { type: 'date', index: true },
        },
      },
    },
  },
};
