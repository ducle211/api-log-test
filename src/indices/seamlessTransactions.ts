import { SEAMLESS_TRANSACTION_INDEX } from '../app/transaction/constants';

export default {
  index: SEAMLESS_TRANSACTION_INDEX,
  _metadata: {
    base64Fields: [],
  },
  body: {
    settings: {
      number_of_shards: 1,
    },
    mappings: {
      properties: {
        id: { type: 'keyword', index: true },
        type: { type: 'keyword', index: true },
        status: { type: 'keyword', index: true },

        //Player
        'player.id': { type: 'keyword', index: true },
        'player.nativeId': { type: 'keyword', index: true },
        'player.playerToken': { type: 'keyword', index: true },

        //Group
        'group.id': { type: 'keyword', index: true },
        'group.name': { type: 'keyword', index: true },

        //Brand
        'brand.id': { type: 'keyword', index: true },
        'brand.name': { type: 'keyword', index: true },

        //Currency
        'currency.id': { type: 'keyword', index: true },
        'currency.name': { type: 'keyword', index: true },
        'currency.code': { type: 'keyword', index: true },
        'currency.symbol': { type: 'keyword', index: true },
        'currency.prefix': { type: 'integer', index: false },

        // Game
        'game.id': { type: 'keyword', index: true },
        'game.name': { type: 'keyword', index: true },
        'game.gameId': { type: 'keyword', index: true },
        'game.gameCode': { type: 'keyword', index: true },

        // Bet
        'bet.id': { type: 'keyword', index: true },
        'bet.amount': { type: 'float', index: false },
        'bet.parentBetId': { type: 'keyword', index: true },

        // Response
        'res.code': { type: 'keyword', index: true },
        'res.message': { type: 'text', index: false },
        'res.balance': { type: 'float', index: false },
        'res.currency': { type: 'keyword', index: true },

        created: { type: 'date', index: true },
      },
    },
  },
};
