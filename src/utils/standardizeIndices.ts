export async function standardizeIndices(elastic, indices, templateName) {
  const existedIndices = (await elastic.cat.indices({ format: 'json', h: ['index'] })).body.map(
    (i) => i.index,
  );

  indices = indices.map((i) => `${templateName}_${i.month}_${i.year}`);

  return existedIndices.filter((value) => indices.includes(value));
}
