const util = require('util');
const exec = util.promisify(require('child_process').exec);

export default {
  name: 'es-dump-cron',
  cronTime: '15 15 11 * *',
  timeZone: 'Asia/Ho_Chi_Minh',
  onTick: async (server) => {
    const {
      config,
      util: { logger },
    } = server;
    const esQuery = JSON.stringify(
      JSON.stringify({ query: { range: { created: { lte: 'now-1y' } } } }),
    );
    await exec(
      `./node_modules/.bin/elasticdump --input=${
        config.ES_URL
      } --output=./dump/transfer_transactions_dump_${new Date()
        .toJSON()
        .slice(0, 10)}.json --searchBody="${esQuery}"`,
    );
    logger.info('[CRON] esdump executed!');
  },
  startWhenReady: true,
};
