export function autoload(libs, dir) {
  require('require-all')({
    dirname: dir,
    filter: /\.js$/,
    recursive: true,
    resolve: function (index) {
      libs.push(index['default']);
    },
  });
}
