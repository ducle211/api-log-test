export function extDate(date?) {
  date = date ? new Date(date) : new Date();
  return {
    month: String(date.getMonth() + 1).padStart(2, '0'),
    year: date.getFullYear(),
  };
}
