import moment from 'moment';

function monthsBetween(start?, end?) {
  const indices = [];
  let dateStart;
  let dateEnd;
  if (start && end) {
    dateStart = moment(start);
    dateEnd = moment(end);
  } else {
  }

  while (dateEnd > dateStart || dateStart.format('M') === dateEnd.format('M')) {
    indices.push({
      month: String(dateStart.month() + 1).padStart(2, '0'),
      year: String(dateStart.year()),
    });
    dateStart.add(1, 'month');
  }
  return indices;
}

export function parseQuery(query) {
  let { limit, page } = query;
  const { filter, sort } = query;

  limit = limit || 25;
  page = page || 1;

  // Must
  let indices = [];
  const mustES = [];
  const sortES = [];
  const regex = /(.+)\|\|\$(cont|eq|between)\|\|(.+)/;

  // Parse filter
  if (filter) {
    filter.map((f) => {
      if (regex.test(f)) {
        const [, key, op, value] = regex.exec(f);
        switch (op) {
          case 'cont':
            mustES.push({
              match_phrase: {
                [key]: (value as string).toLowerCase(),
              },
            });
            break;
          case 'eq':
            mustES.push({
              match: {
                [key]: value,
              },
            });
            break;
          case 'between':
            //eslint-disable-next-line
            const [gte, lte] = value.split(',');
            mustES.push({
              range: {
                [key]: {
                  gte,
                  lte,
                },
              },
            });
            if (key === 'created') {
              indices = monthsBetween(gte, lte);
            }
            break;
        }
      }
    });
  }

  if (!indices?.length) {
    indices = monthsBetween();
  }

  // Parse Sort
  if (sort?.length) {
    sort.map((s) => {
      const [sortField, sortType] = s.split(',');
      sortES.push({
        [sortField]: sortType,
      });
    });
  } else {
    sortES.push({
      created: 'desc',
    });
  }

  return { indices, limit, page, sortES, mustES };
}
