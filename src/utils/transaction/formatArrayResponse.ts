import _ from 'lodash';

import { unflatten } from '../unflatten';

export function formatArrayResponse(body, limit, page, mappings) {
  const { total, hits } = body.hits;

  const pageCount = Math.ceil(total.value / limit);

  return {
    data: _.map(
      _.map(
        _.map(hits, (data) => {
          // @ts-ignore
          return _.partialRight(_.pick, ['_source'])(data)['_source'];
        }),
        (data) => {
          mappings._metadata?.base64Fields?.map((b) => {
            if (data[b]) {
              data[b] = JSON.parse(JSON.parse(Buffer.from(data[b], 'base64').toString('ascii')));
            }
          });
          return data;
        },
      ),
      unflatten,
    ),
    page: +page,
    size: limit,
    pageCount,
    total: total.value,
  };
}
