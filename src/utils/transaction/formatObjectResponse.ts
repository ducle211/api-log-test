import _ from 'lodash';

import { unflatten } from '../unflatten';

export function formatObjectResponse(body, mappings) {
  if (!body?.hits?.hits?.length) {
    return {};
  }

  // @ts-ignore
  const data = _.partialRight(_.pick, ['_source'])(body.hits.hits[0])['_source'];

  mappings._metadata?.base64Fields?.map((b) => {
    if (data[b]) {
      data[b] = JSON.parse(JSON.parse(Buffer.from(data[b], 'base64').toString('ascii')));
    }
  });

  return unflatten(data);
}
