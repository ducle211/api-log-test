import { GetMany } from '../types';

import { SEAMLESS_TRANSACTION_INDEX } from '../constants';
import seamlessMappings from '../../../templates/seamlessTransactions';

export const GET: GetMany = async function (req) {
  const { elastic, util } = this as any;
  const { limit, page, sortES, mustES, indices } = util.parseQuery(req.query);

  const filteredIndices = await util.standardizeIndices(
    elastic,
    indices,
    SEAMLESS_TRANSACTION_INDEX,
  );
  if (!filteredIndices?.length) {
    return { data: [], page: 1, size: '25', pageCount: 0, total: 0 };
  }

  // Query
  const { body } = await elastic.search({
    index: filteredIndices.join(','),
    body: {
      from: (page - 1) * limit,
      size: limit,
      sort: sortES,
      track_total_hits: true,
      query: {
        bool: {
          must: mustES,
        },
      },
    },
  });

  return util.formatArrayResponse(body, limit, page, seamlessMappings);
};
