import { GetOne } from '../types';

import { SEAMLESS_TRANSACTION_INDEX } from '../constants';
import seamlessMappings from '../../../templates/seamlessTransactions';

export const GET: GetOne = async function (req) {
  const { elastic, util } = this as any;

  // Query
  const { body } = await elastic.search({
    index: `${SEAMLESS_TRANSACTION_INDEX}_*`,
    body: {
      query: {
        bool: {
          must: {
            match: {
              id: req.params.id,
            },
          },
        },
      },
    },
  });

  return util.formatObjectResponse(body, seamlessMappings);
};
