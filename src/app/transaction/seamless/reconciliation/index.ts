import { GetMany } from '../../types';
import { SEAMLESS_RECONCILIATION_INDEX } from '../../constants';
import seamlessReconciliationMappings from '../../../../templates/seamlessReconciliation';

export const GET: GetMany = async function (req) {
  const { elastic, util } = this as any;
  const { limit, page, sortES, mustES } = util.parseQuery(req.query);

  // Query
  const { body } = await elastic.search({
    index: `${SEAMLESS_RECONCILIATION_INDEX}_*`,
    body: {
      from: (page - 1) * limit,
      size: limit,
      sort: sortES,
      track_total_hits: true,
      query: {
        bool: {
          must: mustES,
        },
      },
    },
  });

  return util.formatArrayResponse(body, limit, page, seamlessReconciliationMappings);
};
