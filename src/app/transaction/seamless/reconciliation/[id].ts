import { GetOne, UpdateOne } from '../../types';

import { SEAMLESS_RECONCILIATION_INDEX } from '../../constants';
import seamlessReconciliationMappings from '../../../../templates/seamlessReconciliation';

export const GET: GetOne = async function (req) {
  const { elastic, util } = this as any;

  // Query
  const { body } = await elastic.search({
    index: `${SEAMLESS_RECONCILIATION_INDEX}_*`,
    body: {
      query: {
        bool: {
          must: {
            match: {
              id: req.params.id,
            },
          },
        },
      },
    },
  });

  return util.formatObjectResponse(body, seamlessReconciliationMappings);
};

export const PUT: UpdateOne = async function (req, rep) {
  const { elastic, util } = this as any;

  enum Fields {
    status = 'status',
    retryCounter = 'retryCounter',
  }

  const queries = [];
  for (const property in req.body) {
    switch (property) {
      case Fields.status:
        queries.push(`ctx._source['${property}'] = '${req.body[property]}'`);
        break;
      case Fields.retryCounter:
        queries.push(`ctx._source['${property}'] = ${req.body[property]}`);
        break;
      default:
    }
  }

  elastic.updateByQuery({
    index: SEAMLESS_RECONCILIATION_INDEX,
    refresh: true,
    body: {
      script: {
        source: queries.join(';'),
      },
      query: {
        match: req.params,
      },
    },
  });

  rep.code(204);
};
