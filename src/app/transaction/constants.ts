export const SEAMLESS_TRANSACTION_INDEX = 'seamless_transactions';
export const TRANSFER_TRANSACTION_INDEX = 'transfer_transactions';

export const SEAMLESS_TRANSACTION_QUEUE = 'seamless_transactions_queue';
export const TRANSFER_TRANSACTION_QUEUE = 'transfer_transactions_queue';

export const SEAMLESS_RECONCILIATION_INDEX = 'seamless_reconciliation';
export const TRANSFER_RECONCILIATION_INDEX = 'transfer_reconciliation';

export const SEAMLESS_RECONCILIATION_QUEUE = 'seamless_reconciliation_queue';
export const TRANSFER_RECONCILIATION_QUEUE = 'transfer_reconciliation_queue';

export const OPERATOR_FAILURE_INDEX = 'operator_failure';
export const OPERATOR_FAILURE_QUEUE = 'operator_failure_queue';

export const TRANSACTION_VALIDATION_ERRORS = 'transaction_validation_errors';
