import { GetMany } from './types';

export const GET: GetMany = async function () {
  return { service: 'Gamify Log Service', version: '1.0.0' };
};
