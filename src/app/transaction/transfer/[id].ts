import { GetOne } from '../types';

import { TRANSFER_TRANSACTION_INDEX } from '../constants';
import transferMappings from '../../../templates/transferTransactions';

export const GET: GetOne = async function (req) {
  const { elastic, util } = this as any;

  // Query
  const { body } = await elastic.search({
    index: `${TRANSFER_TRANSACTION_INDEX}_*`,
    body: {
      query: {
        bool: {
          must: {
            match: {
              id: req.params.id,
            },
          },
        },
      },
    },
  });

  return util.formatObjectResponse(body, transferMappings);
};
