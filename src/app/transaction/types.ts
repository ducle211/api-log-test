import { NowRequestHandler } from 'fastify-now';

export type GetOne = NowRequestHandler<{ Params: { id: string } }>;

export type GetMany = NowRequestHandler<{ Params: { any } }>;

export type UpdateOne = NowRequestHandler<{
  Params: { id: string };
  Body: any;
}>;
