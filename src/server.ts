import path from 'path';
import fastify from 'fastify';
import now from 'fastify-now';
import pino from 'pino';
import loadConfig from './lib/config';
import { standardizeIndices } from './utils/standardizeIndices';
import { autoload } from './utils/autoload';
import { parseQuery } from './utils/parseQuery';
import { formatArrayResponse } from './utils/transaction/formatArrayResponse';
import { formatObjectResponse } from './utils/transaction/formatObjectResponse';

const { parsed } = loadConfig();
const logger = pino({
  transport: {
    target: 'pino-pretty',
  },
});

export async function createServer() {
  const server = fastify({
    ignoreTrailingSlash: true,
    logger: {
      level: process.env.LOG_LEVEL,
    },
  });

  server.decorate('config', parsed);

  server.decorate('util', {
    logger,
    parseQuery,
    standardizeIndices,
    formatArrayResponse,
    formatObjectResponse,
  });

  await server.register(require('fastify-qs'), {});

  // Register cron auto dump & clean ES
  // const crons = [];
  // autoload(crons, `${__dirname}/utils/cron`);
  // await server.register(require('./lib/cron'), {
  //   jobs: crons,
  // });

  // Register Swagger plugin
  await server.register(require('fastify-swagger'), {
    routePrefix: '/documentation',
    swagger: {
      info: {
        title: 'Gamify Log Swagger',
        description: 'Gamify Log SwaggerAPI',
        version: '1.0.0',
      },
      host: parsed.HOST_NAME,
      schemes: ['https'],
      consumes: ['application/json'],
      produces: ['application/json'],
      securityDefinitions: {
        signature: {
          type: 'apiKey',
          name: 'signature',
          in: 'header',
        },
      },
    },
    uiConfig: {
      docExpansion: 'full',
      deepLinking: false,
      validatorUrl: null,
    },
    staticCSP: true,
    transformStaticCSP: (header) => header,
    exposeRoute: true,
  });
  logger.info('[Swagger] Plugin registered!');

  // Auto register routes in /app
  await server.register(now, {
    routesFolder: path.join(__dirname, './app'),
  });

  // Auto load indices in /indices
  const indices = [];
  autoload(indices, `${__dirname}/templates`);

  // Register ES plugin
  await server.register(require('./lib/elasticsearch'), {
    indices,
    node: parsed.ES_URL,
  });
  logger.info('[ElasticSearch] Plugin registered and connected!');

  // Auto load consumers in /consumers
  const consumers = [];
  autoload(consumers, `${__dirname}/consumers`);

  // Register AMQP plugin
  await server.register(require('./lib/amqp'), {
    amqpURL: parsed.AMQP_URL,
    queues: consumers,
    elastic: server['elastic'],
  });
  logger.info('[AMQP] Plugin registered and connected!');

  await server.ready();

  return server;
}

export async function startServer() {
  process.on('unhandledRejection', (err) => {
    console.error(err);
    process.exit(1);
  });

  const server = await createServer();
  await server.listen(+process.env.API_PORT, process.env.API_HOST);

  for (const signal of ['SIGINT', 'SIGTERM']) {
    process.on(signal, () =>
      server.close().then((err) => {
        console.log(`close application on ${signal}`);
        process.exit(err ? 1 : 0);
      }),
    );
  }
}

if (process.env.NODE_ENV !== 'test') {
  startServer();
}
