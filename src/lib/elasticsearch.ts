'use strict';

// @ts-ignore
const fp = require('fastify-plugin');
import _ from 'lodash';
import { Client } from '@elastic/elasticsearch';

async function fastifyElasticsearch(fastify, options) {
  const { namespace, healthcheck } = options;
  delete options.namespace;
  delete options.healthcheck;

  const client: Client = options.client || new Client(options);

  options.indices.map(async (template) => {
    const { body } = await client.indices.existsIndexTemplate({ name: template.name });
    if (!body) {
      await client.indices.putIndexTemplate(_.omit(template, ['_metadata']) as any);
    }
  });

  if (healthcheck !== false) {
    await client.ping();
  }

  if (namespace) {
    if (!fastify.elastic) {
      fastify.decorate('elastic', {});
    }

    if (fastify.elastic[namespace]) {
      throw new Error(`Elasticsearch namespace already used: ${namespace}`);
    }

    fastify.elastic[namespace] = client;

    fastify.addHook('onClose', (instance, done) => {
      instance.elastic[namespace].close(done);
    });
  } else {
    fastify.decorate('elastic', client).addHook('onClose', (instance, done) => {
      instance.elastic.close(done);
    });
  }
}

module.exports = fp(fastifyElasticsearch, {
  fastify: '3.x',
  name: 'fastify-elasticsearch',
});
