'use strict';

// @ts-ignore
const fp = require('fastify-plugin');
import amqpClient from 'amqplib';

async function fastifyAmqp(fastify, options) {
  let connection;

  try {
    connection = await amqpClient.connect(options.amqpURL, options.socket);
  } catch (err) {
    throw new Error(err);
  }

  fastify.addHook('onClose', () => connection.close());

  options.queues.map(async (queue) => {
    for (let i = 0; i < queue.num; i++) {
      const channel = await connection.createChannel();
      channel.assertQueue(queue.name).then(function () {
        return channel.consume(queue.name, function (msg) {
          return queue.handler(channel, msg, options.elastic);
        });
      });
    }
  });

  fastify.decorate('amqp', {
    connection,
  });
}

module.exports = fp(fastifyAmqp, {
  fastify: '>=1.0.0',
  name: 'fastify-amqp',
});
