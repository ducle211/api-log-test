import Joi from 'joi';

export default Joi.object({
  id: Joi.string().uuid().required(),

  nativeId: Joi.string().required(),

  nativeTransactionId: Joi.string().required(),

  brandCode: Joi.string().required(),

  groupCode: Joi.string().required(),

  currencyCode: Joi.string().required(),

  amount: Joi.number().required(),

  type: Joi.string().valid('DEPOSIT', 'WITHDRAW').required(),

  reason: Joi.string().optional(),

  created: Joi.date().required(),
});
