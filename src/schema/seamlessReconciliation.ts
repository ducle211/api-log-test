import Joi from 'joi';

export default Joi.object({
  id: Joi.string().uuid().required(),

  transactionId: Joi.string().required(),

  bet: Joi.object({
    id: Joi.string().uuid().required(),
    amount: Joi.number().required(),
    parentBetId: Joi.string().uuid().allow(null, ''),
  }),

  game: Joi.object({
    id: Joi.string().uuid().required(),
    name: Joi.string().required(),
    gameId: Joi.string().length(4).required(),
    gameCode: Joi.string().required(),
  }),

  group: Joi.object({
    id: Joi.string().uuid().required(),
    name: Joi.string().required(),
  }),

  brand: Joi.object({
    id: Joi.string().uuid().required(),
    name: Joi.string().required(),
  }),

  type: Joi.string().valid('PAYOUT').required(),

  player: Joi.object({
    id: Joi.string().uuid().required(),
    nativeId: Joi.string().required(),
    playerToken: Joi.string().required(),
  }),

  currency: Joi.object({
    id: Joi.string().uuid().required(),
    name: Joi.string().required(),
    code: Joi.string().required(),
    symbol: Joi.string().required(),
    prefix: Joi.number().required(),
  }),

  payload: Joi.string().required(),

  status: Joi.string().valid('PENDING', 'RESOLVED').required(),

  retryCounter: Joi.number().integer().min(0).required(),

  created: Joi.date().required(),

  updated: Joi.date().required(),
});
