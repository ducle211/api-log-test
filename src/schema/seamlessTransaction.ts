import Joi from 'joi';

export default Joi.object({
  id: Joi.string().uuid().required(),

  type: Joi.string().valid('PLACE_BET', 'PAYOUT').required(),

  status: Joi.string().valid('SUCCEED', 'FAILED').required(),

  player: Joi.object({
    id: Joi.string().uuid().required(),
    nativeId: Joi.string().required(),
    playerToken: Joi.string().required(),
  }),

  group: Joi.object({
    id: Joi.string().uuid().required(),
    name: Joi.string().required(),
  }),

  brand: Joi.object({
    id: Joi.string().uuid().required(),
    name: Joi.string().required(),
  }),

  currency: Joi.object({
    id: Joi.string().uuid().required(),
    name: Joi.string().required(),
    code: Joi.string().required(),
    symbol: Joi.string().required(),
    prefix: Joi.number().required(),
  }),

  game: Joi.object({
    id: Joi.string().uuid().required(),
    name: Joi.string().required(),
    gameId: Joi.string().length(4).required(),
    gameCode: Joi.string().required(),
  }),

  bet: Joi.object({
    id: Joi.string().uuid().required(),
    amount: Joi.number().required(),
    parentBetId: Joi.string().uuid().required(),
  }),

  res: Joi.object({
    code: Joi.number().required(),
    message: Joi.string().required(),
    balance: Joi.number().required(),
    currency: Joi.string().required(),
  }),

  created: Joi.date().required(),
});
