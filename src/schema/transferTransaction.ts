import Joi from 'joi';

export default Joi.object({
  id: Joi.string().uuid().required(),

  nativeTransactionId: Joi.string().required(),

  type: Joi.string().valid('DEPOSIT', 'WITHDRAW').required(),

  amount: Joi.number().required(),

  status: Joi.string().valid('SUCCEED', 'FAILED').required(),

  note: Joi.string().optional(),

  player: Joi.object({
    id: Joi.string().uuid().required(),
    nativeId: Joi.string().required(),
  }),

  group: Joi.object({
    id: Joi.string().uuid().required(),
    name: Joi.string().required(),
  }),

  brand: Joi.object({
    id: Joi.string().uuid().required(),
    name: Joi.string().required(),
  }),

  wallet: Joi.object({
    id: Joi.string().uuid().required(),
    balance: Joi.number().required(),
  }),

  currency: Joi.object({
    id: Joi.string().uuid().required(),
    name: Joi.string().required(),
    code: Joi.string().required(),
    symbol: Joi.string().required(),
    prefix: Joi.number().required(),
  }),

  created: Joi.date().required(),
});
