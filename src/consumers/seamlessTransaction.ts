import { flattenObject } from '../utils/flatten';
import { extDate } from '../utils/extDate';
import {
  SEAMLESS_TRANSACTION_INDEX,
  SEAMLESS_TRANSACTION_QUEUE,
  TRANSACTION_VALIDATION_ERRORS,
} from '../app/transaction/constants';
import seamlessTransactionSchema from '../schema/seamlessTransaction';

const queueConfig = {
  name: SEAMLESS_TRANSACTION_QUEUE,
  num: 5,
};

const handler = async (channel, msg, elastic) => {
  if (msg === null) {
    channel.ack(msg);
    return;
  }

  try {
    const transaction = JSON.parse(msg.content.toString());
    const { error } = seamlessTransactionSchema.validate(transaction);
    if (error) {
      throw error;
    }

    const { month, year } = extDate();
    await elastic.index({
      index: `${SEAMLESS_TRANSACTION_INDEX}_${month}_${year}`,
      body: flattenObject(transaction),
    });
  } catch (error) {
    elastic.index({
      index: TRANSACTION_VALIDATION_ERRORS,
      body: {
        type: SEAMLESS_TRANSACTION_INDEX,
        message: JSON.stringify(error),
        payload: msg.content.toString(),
      },
    });
  } finally {
    channel.ack(msg);
  }
};

export default {
  ...queueConfig,
  handler,
};
