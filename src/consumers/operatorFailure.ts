import { flattenObject } from '../utils/flatten';
import { extDate } from '../utils/extDate';
import {
  OPERATOR_FAILURE_INDEX,
  OPERATOR_FAILURE_QUEUE,
  TRANSACTION_VALIDATION_ERRORS,
} from '../app/transaction/constants';
import operatorFailureSchema from '../schema/operatorFailure';

const queueConfig = {
  name: OPERATOR_FAILURE_QUEUE,
  num: 5,
};

const handler = async (channel, msg, elastic) => {
  if (msg === null) {
    channel.ack(msg);
    return;
  }

  try {
    const transaction = JSON.parse(msg.content.toString());
    const { error } = operatorFailureSchema.validate(transaction);
    if (error) {
      throw error;
    }

    const { month, year } = extDate();
    await elastic.index({
      index: `${OPERATOR_FAILURE_INDEX}_${month}_${year}`,
      body: flattenObject(transaction),
    });
  } catch (error) {
    console.log(error);
    elastic.index({
      index: TRANSACTION_VALIDATION_ERRORS,
      body: {
        type: OPERATOR_FAILURE_INDEX,
        message: JSON.stringify(error),
        payload: msg.content.toString(),
      },
    });
  } finally {
    channel.ack(msg);
  }
};

export default {
  ...queueConfig,
  handler,
};
