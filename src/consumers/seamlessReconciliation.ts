import { flattenObject } from '../utils/flatten';
import { extDate } from '../utils/extDate';
import {
  SEAMLESS_RECONCILIATION_INDEX,
  SEAMLESS_RECONCILIATION_QUEUE,
  TRANSACTION_VALIDATION_ERRORS,
} from '../app/transaction/constants';
import seamlessReconciliationSchema from '../schema/seamlessReconciliation';

const queueConfig = {
  name: SEAMLESS_RECONCILIATION_QUEUE,
  num: 5,
};

const handler = async (channel, msg, elastic) => {
  if (msg === null) {
    channel.ack(msg);
    return;
  }

  try {
    const transaction = JSON.parse(msg.content.toString());
    const { error } = seamlessReconciliationSchema.validate(transaction);
    if (error) {
      throw error;
    }

    const { month, year } = extDate();
    await elastic.index({
      index: `${SEAMLESS_RECONCILIATION_INDEX}_${month}_${year}`,
      body: flattenObject(transaction),
    });
  } catch (error) {
    console.log(error);
    elastic.index({
      index: TRANSACTION_VALIDATION_ERRORS,
      body: {
        type: SEAMLESS_RECONCILIATION_INDEX,
        message: JSON.stringify(error),
        payload: msg.content.toString(),
      },
    });
  } finally {
    channel.ack(msg);
  }
};

export default {
  ...queueConfig,
  handler,
};
