/* eslint-disable */
const { Client } = require('@elastic/elasticsearch');

const client = new Client({ node: 'http://localhost:9200' });

client.indices
  .delete({
    index: 'transfer_transactions',
  })
  .then(
    function (resp) {
      console.log('Deleted successfully! Please restart application to reinit the index!');
    },
    function (err) {
      console.trace(err.message);
    },
  );
