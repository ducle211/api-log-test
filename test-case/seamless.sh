status_code=$(curl -s -o /dev/null -w "%{http_code}" https://api-regression.dev.game.topasianplatform.com/api/regression-test/seamless?env=pre-staging)

if [[ "$status_code" == 200 ]] ; then
  echo "Seamless check ok! $status_code" 
else
  echo "$status_code"
  exit 1
fi
