status_code=$(curl -s -o /dev/null -w "%{http_code}" https://api-regression.dev.game.topasianplatform.com/api/regression-test/funmode?env=pre-staging)

if [[ "$status_code" == 200 ]] ; then
  echo "Fun mode check ok! $status_code" 
else
  echo "$status_code"
  exit 1
fi
