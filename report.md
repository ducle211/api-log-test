git add .
git commit -m "Update"
git push


test:
  stage: test
  script:
    - >
      if $CI_COMMIT_REF_NAME == "pre-staging"; then
        echo "Pre-staging"
        chmod +x test-case/funmode.sh
        ./test-case/funmode.sh
      if $CI_COMMIT_REF_NAME == "staging"; then
        echo "Staging"
        chmod +x test-case/stg-funmod.sh
        ./test-case/stg-funmod.sh
      fi

    # - chmod +x check_status.sh
    # - bash check_status.sh
  allow_failure: true