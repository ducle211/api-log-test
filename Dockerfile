FROM node:17-alpine AS deps
# Check https://github.com/nodejs/docker-node/tree/b4117f9333da4138b03a546ec926ef50a31506c3#nodealpine to understand why libc6-compat might be needed.
RUN apk add --no-cache libc6-compat
WORKDIR /app
COPY package.json pnpm-lock.yaml ./
RUN npm install -g pnpm
RUN pnpm install --no-frozen-lockfile

FROM node:17-alpine AS builder
ARG APP_ENV
WORKDIR /app
COPY . .
COPY .env.$APP_ENV ./
COPY --from=deps /app/node_modules ./node_modules
RUN npm run build

FROM node:17-alpine AS runner
WORKDIR /usr/app
ARG APP_ENV
COPY --from=builder /app/build ./build
COPY package.json ./
COPY .env.$APP_ENV ./
RUN npm install -g pnpm
RUN pnpm install 
USER node
EXPOSE 5000
ENV NODE_ENV=$APP_ENV
CMD ["npm", "start"]
