status_code=$(curl -s -o /dev/null -w "%{http_code}" https://api-regression.dev.game.topasianplatform.com/)

if [[ "$status_code" == 200 ]] ; then
  echo "Site status changed $status_code" 
else
  echo "$status_code"
  exit 1
fi
